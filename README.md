# Alice

Django e-commerce

Python version: 3.10.1
    |- requirements.txt
RabbitMQ version: 3.9.11 (Don't forget. Use cmd as admin to manage rabbitmq server.)
    |- Dependencies: Erlang/OTP 24

Run Celery worker: 
    celery -A core worker -l info -P solo
Run Flower:
    celery -A core flower