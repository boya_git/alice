from decimal import Decimal
from django.conf import settings
from shop.models import Product


class Cart(object):

    def __init__(self, request):
        """
            Init the product cart.
        """
        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_ID)
        if not cart:
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart
        ### TODO ?
        self.error = False

    def __iter__(self):
        """
            Iterate over the items and get products from the DB.
        """
        p_ids = self.cart.keys()
        products = Product.objects.filter(id__in=p_ids)
        cart = self.cart.copy()

        for product in products:
            p_id = str(product.id)
            cart[p_id]['product'] = product

            if not product.available:
                cart[p_id]['error'] = f'Продукт более недоступен.'
                self.error = True
            elif cart[p_id]['quantity'] > product.quantity:
                cart[p_id]['error'] = f'Указанное количество товаров {cart[p_id]["quantity"]}\n' \
                                      f'Превышает доступное в магазине {product.quantity}\n' \
                                      f'Пожалуйста, укажите новое значение.'
                self.error = True

        for item in cart.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price']*item['quantity']
            yield item

    def __len__(self):
        return sum(item['quantity'] for item in self.cart.values())

    def get(self, product):
        p_id = str(product.id)
        return self.cart[p_id]

    def add(self, product, quantity=1, override_quantity=False):
        p_id = str(product.id)
        if p_id not in self.cart:
            self.cart[p_id] = {'quantity': 0,
                               'price': str(product.price),
            }
        if override_quantity:
            self.cart[p_id]['quantity'] = quantity
        else:
            self.cart[p_id]['quantity'] += quantity
        self.save()

    def remove(self, product):
        p_id = str(product.id)
        if p_id in self.cart:
            del self.cart[p_id]
            self.save()

    def get_total_price(self):
        return sum(Decimal(item['price'])* item['quantity'] for item in self.cart.values())

    def clear(self):
        del self.session[settings.CART_SESSION_ID]
        self.save()

    def save(self):
        self.session.modified = True
