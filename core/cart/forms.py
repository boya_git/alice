from django import forms


class CartAddProductForm(forms.Form):

    def __init__(self, *args, **kwargs, ):
        product_quantity_choices = kwargs.pop('product_quantity_choices', ())
        super().__init__(*args, **kwargs)
        if product_quantity_choices:
            self.fields['quantity'].choices = product_quantity_choices

    quantity = forms.TypedChoiceField(choices=[(1, '1')], coerce=int,)
    override = forms.BooleanField(required=False,
                                  initial=False,
                                  widget=forms.HiddenInput)
