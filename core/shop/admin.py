from django.contrib import admin
from .models import Category, Product, ShopContacts, ShopEmail, ShopAddress, ShopPhoneNumber


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', ]
    prepopulated_fields = {'slug': ('name', )}


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'quantity', 'price',
                    'available', 'created', 'updated']
    list_filter = ['available', 'created', 'updated', ]
    list_editable = ['quantity', 'price', 'available', ]
    prepopulated_fields = {'slug': ('name', )}


class ShopAddressInline(admin.StackedInline):
    model = ShopAddress
    extra = 1


class ShopEmailInline(admin.StackedInline):
    model = ShopEmail
    extra = 1


class ShopPhoneNumberInline(admin.StackedInline):
    model = ShopPhoneNumber
    extra = 1


@admin.register(ShopContacts)
class ShopContactsAdmin(admin.ModelAdmin):
    inlines = [
        ShopAddressInline,
        ShopEmailInline,
        ShopPhoneNumberInline,
    ]

    def has_add_permission(self, request):
        if ShopContacts.objects.exists():
            return False
        return True