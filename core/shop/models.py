from django.db import models
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField


class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, unique=True)
    image = models.ImageField(upload_to='categorys/%Y/%m/%d', blank=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def get_absolute_url(self):
        return reverse('shop:product_list_by_category',
                       args=[self.slug])

    def __str__(self):
        return self.name


class Product(models.Model):
    category = models.ForeignKey(Category,
                                 related_name='products',
                                 on_delete=models.CASCADE)
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True)
    image = models.ImageField(upload_to='products/%Y/%m/%d', blank=True)
    description = models.TextField(blank=True)
    quantity = models.PositiveIntegerField(default=1)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    available = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('name',)
        index_together = (('id', 'slug'),)
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    def get_absolute_url(self):
        return reverse('shop:product_detail',
                       args=[self.id, self.slug])

    def quantity_choices(self):
        return [(i, str(i)) for i in range(1, self.quantity+1)]

    def __str__(self):
        return f'{self.name} | {self.price}'

    def save(self, *args, **kwargs):
        if self.quantity <= 0:
            self.available = False
        super().save(*args, **kwargs)


class ShopContacts(models.Model):
    name = models.CharField(max_length=200, default='Контактная информация')

    class Meta:
        verbose_name = 'Контакты'
        verbose_name_plural = 'Контакты'

    def __str__(self):
        return 'Контактная информация'


class ShopEmail(models.Model):
    shop = models.ForeignKey(ShopContacts,
                             related_name='emails',
                             on_delete=models.CASCADE)
    email = models.EmailField(verbose_name='Эл. почта', )
    desc = models.CharField(verbose_name='Краткое описание', max_length=250,
                            unique=False, null=True, blank=True)

    class Meta:
        verbose_name = 'Эл. почта'
        verbose_name_plural = 'Эл. почты'

    def __str__(self):
        return f'{self.email}'


class ShopPhoneNumber(models.Model):
    shop = models.ForeignKey(ShopContacts,
                             related_name='phones',
                             on_delete=models.CASCADE)
    phone = PhoneNumberField(verbose_name='Номер телефона',
                             unique=False, null=False, blank=False)

    class Meta:
        verbose_name = 'Тел. номер'
        verbose_name_plural = 'Тел. номера'

    def __str__(self):
        return f'{self.phone}'


class ShopAddress(models.Model):
    shop = models.ForeignKey(ShopContacts,
                             related_name='addresses',
                             on_delete=models.CASCADE)
    country = models.CharField(verbose_name='Страна', max_length=50,
                               unique=False, null=False, blank=False)
    city = models.CharField(verbose_name='Город', max_length=50,
                            unique=False, null=False, blank=False)
    street = models.CharField(verbose_name='Улица', max_length=100,
                              unique=False, null=False, blank=False)
    building = models.CharField(verbose_name='Дом/Строение', max_length=10,
                                unique=False, null=False, blank=False)
    apartment = models.CharField(verbose_name='Квартира/Комната', max_length=10,
                                 unique=False, null=True, blank=True)
    postal_code = models.CharField(verbose_name='Почтовый индекс', max_length=10,
                                   unique=False, null=True, blank=True)

    class Meta:
        verbose_name = 'Адрес'
        verbose_name_plural = 'Адреса'

    def __str__(self):
        return f'{self.country}, {self.city}, ул. {self.street}, {self.building}/{self.apartment}, {self.postal_code}'
