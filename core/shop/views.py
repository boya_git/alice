from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage

from cart.forms import CartAddProductForm
from .models import Category, Product
from .services import get_most_filled_categories


def index(request):
    context = {
        'most_filed_categories': get_most_filled_categories(3),
    }
    return render(request, 'shop/index.html', context=context)


def about(request):
    return render(request, 'shop/about.html', {})


def info(request):
    return render(request, 'shop/info.html', {})


def contacts(request):
    return render(request, 'shop/contacts.html', {})


def product_list(request, category_slug=None):
    sort = request.GET.get('sort')
    page_no = int(request.GET.get('page', 1))
    categories = Category.objects.all()

    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = category.products.all().filter(available=True)
    else:
        category = None
        products = Product.objects.filter(available=True)

    if sort:
        products = sorted(products,
                          key=lambda product: product.price,
                          reverse=sort=='desc')

    paginator = Paginator(products, 16)
    try:
        page = paginator.page(page_no)
    except EmptyPage:
        page_no = 1
        page = paginator.page(page_no)
    pages_around = list(filter(lambda x: x > 0 and x <= paginator.num_pages, range(page_no-5//2, page_no+5//2+1)))

    return render(request,
                  'shop/product/list.html',
                  {'category': category,
                   'categories': categories,
                   'products': page.object_list,
                   'page': page,
                   'pages_around': pages_around,
                   'sort': sort})


def product_detail(request, id, slug):
    product = get_object_or_404(Product,
                                id=id,
                                slug=slug,
                                available=True)
    cart_product_form = CartAddProductForm(product_quantity_choices=product.quantity_choices())
    return render(request,
                  'shop/product/detail.html',
                  {'product': product,
                   'cart_product_form': cart_product_form,
                   'is_incart': 0, })
