from django import template

register = template.Library()


@register.filter
def is_incart(cart, product):
    for item in cart:
        if item['product'] == product:
            return True
    return False
