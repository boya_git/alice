from django.urls import path

from . import views

app_name = 'shop'

urlpatterns = [
    path('', views.index, name='index'),
    path('about', views.about, name='about'),
    path('info', views.info, name='info'),
    path('contacts', views.contacts, name='contacts'),
    # TODO rename to 'catalog'?
    path('catalog/', views.product_list, name='product_list'),
    # TODO rename to 'category'?
    path('catalog/<slug:category_slug>/', views.product_list, name='product_list_by_category'),
    path('product/<int:id>/<slug:slug>/', views.product_detail, name='product_detail'),
]
