from django.db.models import Count, QuerySet

from .models import Category


def get_most_filled_categories(number: int) -> QuerySet:
    categories_by_products_count = Category.objects.annotate(products_count=Count('products')).order_by('-products_count')
    most_filled = categories_by_products_count[:number]
    return most_filled


