from core import celery_app
from django.conf import settings
from django.core.mail import send_mail

from .models import Order


@celery_app.task
def order_created(order_id: int):
    """
        Task to send an email notification when an
        order is successfully created.
    """
    order = Order.objects.get(id=order_id)
    subject = f'Заказ номер {order.id}'
    message = f'Уважаемый {order.first_name},\n\n' \
              f'Ваш заказ на сумму {order.get_total_cost()} BYN успешно создан.\n\n' \
              f'Номер вашего заказа: {order.id}\n\n' \
              f'Скоро мы свяжемся с вами для уточнения деталей.'
    mail_sent = send_mail(subject,
                          message,
                          settings.EMAIL_HOST_USER,
                          [order.email, ])
    return mail_sent
