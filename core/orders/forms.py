from django import forms

from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget

from .models import Order


class OrderCreateForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['first_name', 'last_name', 'email', 'city',
                  'phone', 'address', 'postal_code']

    phone = PhoneNumberField(widget=PhoneNumberPrefixWidget(initial='BY'))
    phone.error_messages['invalid'] = 'Некоректный номер телефона! Выберите код страны и\n' \
                                      'используйте формат 29/33 XXX-XX-XX в зависимости от вашего оператора.'

    def __init__(self, *args, **kwargs):
        super(OrderCreateForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
