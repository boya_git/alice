from cart.cart import Cart
from django.contrib import messages
from django.shortcuts import redirect, render

from .forms import OrderCreateForm
from .models import OrderItem
from .tasks import order_created


def order_create(request):
    cart = Cart(request)
    if not cart:
        messages.info(request, f'Корзина пуста, оформлять нечего.')
        return redirect('cart:cart_detail')

    for item in cart:
        if 'error' in item.keys():
            messages.info(request, f'Невозможно создать заказ.\n'
                                   f'Детальная информацая отображена в товарах в соответствуюзщих столбцах')
            return redirect('cart:cart_detail')
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            order = form.save()
            for item in cart:
                OrderItem.objects.create(order=order,
                                         product=item['product'],
                                         price=item['price'],
                                         quantity=item['quantity'], )
                item['product'].quantity -= item['quantity']
                item['product'].save()
            # cuz order creation was successful in this moment
            cart.clear()
            order_created.delay(order.id)
            return render(request,
                          'orders/order/created.html',
                          {'order': order})
    else:
        form = OrderCreateForm()
    return render(request,
                  'orders/order/create.html',
                  {'cart': cart,
                   'form': form})
