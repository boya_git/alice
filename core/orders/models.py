from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from shop.models import Product



ORDER_STATUS = [
    ('New', 'Новый'),
    ('In processing', 'В работе'),
    ('Ready', 'Готов'),
    ('Complete', 'Завершён'),
    ('Canceled', 'Отменён')
]


class Order(models.Model):
    first_name = models.CharField(verbose_name='Имя', max_length=50)
    last_name = models.CharField(verbose_name='Фамилия', max_length=50)
    email = models.EmailField(verbose_name='Эл. почта', )
    phone = PhoneNumberField(verbose_name='Номер телефона',
                             unique=False, null=False, blank=False,
                             default='+375291111111')
    address = models.CharField(verbose_name='Ул. Дом. Кв.', max_length=250)
    postal_code = models.CharField(verbose_name='Почтовый код', max_length=20)
    city = models.CharField(verbose_name='Город', max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=15, choices=ORDER_STATUS, default='New')
    paid = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created', )
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return f'Order {self.id}'

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.items.all())


class OrderItem(models.Model):
    order = models.ForeignKey(Order,
                              related_name='items',
                              on_delete=models.CASCADE)
    product = models.ForeignKey(Product,
                                related_name='order_items',
                                on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return f'Order Item {self.id}'

    def get_cost(self):
        return self.price * self.quantity
